Qunit.requireCss('unit-tests/atlassian-js/atlassian-js-test.css');
Qunit.require('js/external/jquery/jquery.js');
Qunit.require('js/atlassian/atlassian.js');


module("Unit Tests for atlassian.js");

test("AJS.version", function() {
    ok( typeof AJS.version === "string", " AJS.version should return a string (in REFAPP it will be the generic project.version string)");
});

test("AJS.alphanum", function() {
    function assertAlphaNum(a, b, expected) {
        var actual = AJS.alphanum(a, b);
        equal(actual, expected, "alphanum test\n" + a + "\n" + b);

        // try in reverse
        actual = AJS.alphanum(b, a);
        equal(actual, expected * -1, "alphanum (reverse) test\n" + b + "\n" + a)
    }

    assertAlphaNum("a", "a", 0);
    assertAlphaNum("a", "b", -1);
    assertAlphaNum("b", "a", 1);

    assertAlphaNum("a0", "a1", -1);
    assertAlphaNum("a10", "a1", 1);
    assertAlphaNum("a2", "a1", 1);
    assertAlphaNum("a2", "a10", -1);
});

test("AJS.escapeHtml", function() {

    equal(AJS.escapeHtml("a \" doublequote"), "a &quot; doublequote");
    equal(AJS.escapeHtml("a ' singlequote"), "a &#39; singlequote");
    equal(AJS.escapeHtml("a < lessthan"), "a &lt; lessthan");
    equal(AJS.escapeHtml("a > greaterthan"), "a &gt; greaterthan");
    equal(AJS.escapeHtml("a & ampersand"), "a &amp; ampersand");
    equal(AJS.escapeHtml("a ` accent grave"), "a &#96; accent grave");

    equal(AJS.escapeHtml("foo"), "foo");

    equal(AJS.escapeHtml("<foo>"), "&lt;foo&gt;");
    equal(AJS.escapeHtml("as<foo>as"), "as&lt;foo&gt;as");

    equal(AJS.escapeHtml("some <input class=\"foo\" value='bar&wombat'> thing"), "some &lt;input class=&quot;foo&quot; value=&#39;bar&amp;wombat&#39;&gt; thing");
});

test("AJS.isClipped", function() {
    equal(AJS.isClipped( AJS.$("#shouldBeClipped")), true, "Should be clipped");
    equal(AJS.isClipped( AJS.$("#shouldNotBeClipped")), false, "Should not be clipped");
});

module("AJS.toInit", {
    // Overwrite jquery's ready() behaviour to assert that the functions
    // end up in the right place and to manually trigger them.
    setup: function() {
        this.readyList = [];
        this.oldJqueryReady = AJS.$.ready;
        AJS.$.ready = function(func) {
            this.readyList.push(func);
        };

        this.triggerReady = function() {
            for (var i = 0; i < this.readyList.length; i++) {
                this.readyList[i]();
            }
        };
    },

    teardown: function() {
        // restore actual ready function
        AJS.$.ready = this.oldJqueryReady;
    }
});
test("Add multiple functions", function() {
    var func1 = sinon.spy();
    var func2 = sinon.spy();

    AJS.toInit(func1);
    AJS.toInit(func2);
    this.triggerReady();

    ok(func1.calledOnce, "should have been called");
    ok(func2.calledOnce, "should have been called");
});

test("Throw error", function() {

    var func1 = sinon.stub().throws("WTF");
    var func2 = sinon.spy();

    AJS.toInit(func1);
    AJS.toInit(func2);
    this.triggerReady();

    ok(func1.threw, "should have thrown an exception");
    ok(func2.calledOnce, "should have been called");
});