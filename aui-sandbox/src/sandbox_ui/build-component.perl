#!/usr/bin/perl
use strict;
use warnings;

my $master = "(function(){\n";

my $jsdirectory = 'static/template/js/';
opendir (JSDIR, $jsdirectory) or die $!;

while (my $file = readdir(JSDIR)) {
   if($file ne "." && $file ne "..") {
      print "Building $file...\n";
      open (MYFILE, "static/template/js/".$file);
      $file =~ s/.js//g;
      my $content = "var js = ''\n";
      while (<MYFILE>) {
         chomp;
         $_;
         s/\r//g;
         s/\'/\\\'/g;
         $content = $content." + '".$_."\\n'\n";
      }
      close (MYFILE); 
      $content= $content."SANDBOX.Library.register('$file', 'js', js, 'components')\n\n";
      $master = $master.$content;
      # print $content;
   }
}

my $htmldirectory = 'static/template/html/';
opendir (HTMLDIR, $htmldirectory) or die $!;

while (my $file = readdir(HTMLDIR)) {
   if($file ne "." && $file ne "..") {
      print "Building $file...\n";
      open (MYFILE, "static/template/html/".$file);
      $file =~ s/.html//g;
      my $content = "var html = ''\n";
      while (<MYFILE>) {
         chomp;
         $_;
         s/\r//g;
         s/\'/\\\'/g;
         $content = $content." + '".$_."\\n'\n";
      }
      close (MYFILE); 
      $content= $content."SANDBOX.Library.register('$file', 'html', html, 'components')\n\n";
      $master = $master.$content;
       # print $content;
   }
}

my $patternshtmldirectory = 'static/template/patterns/html/';

my $isPatternHtml = opendir (PATTERNSHTMLDIR, $patternshtmldirectory);

if($isPatternHtml){
   while (my $file = readdir(PATTERNSHTMLDIR)) {
      if($file ne "." && $file ne "..") {
         print "Building $file...\n";
         open (MYFILE, "static/template/patterns/html/".$file);
         $file =~ s/.html//g;
         my $content = "var html = ''\n";
         while (<MYFILE>) {
            chomp;
            $_;
            s/\r//g;
            s/\'/\\\'/g;
            $content = $content." + '".$_."\\n'\n";
         }
         close (MYFILE); 
         $content= $content."SANDBOX.Library .register('$file', 'html', html, 'patterns')\n\n";
         $master = $master.$content;
          # print $content;
      }
   }   
}


my $patternsjsdirectory = 'static/template/patterns/js/';
my $isPatternJs = opendir (PATTERNSJSDIR, $patternsjsdirectory);

if($isPatternJs){
   while (my $file = readdir(PATTERNSJSDIR)) {
      if($file ne "." && $file ne "..") {
         print "Building $file...\n";
         open (MYFILE, "static/template/patterns/js/".$file);
         $file =~ s/.js//g;
         my $content = "var js = ''\n";
         while (<MYFILE>) {
            chomp;
            $_;
            s/\r//g;
            s/\'/\\\'/g;
            $content = $content." + '".$_."\\n'\n";
         }
         close (MYFILE); 
         $content= $content."SANDBOX.Library.register('$file', 'js', js, 'patterns')\n\n";
         $master = $master.$content;
         # print $content;
      }
   }
}

$master = $master."})();";
my $outfile = "static/template/registerall.js";
open (OUTFILE, "> $outfile");
 print OUTFILE "$master";
close(OUTFILE);
