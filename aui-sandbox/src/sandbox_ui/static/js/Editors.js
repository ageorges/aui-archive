var Editors = {

	config: {
		html: {
			id: 'html-editor',
			mode: 'ace/mode/html'
		},
		js: {
			id: 'js-editor',
			mode: 'ace/mode/javascript'
		},
		css: {
			id: 'css-editor',
			mode: 'ace/mode/css'
		}
	},

	commands: {
		save: {
			name: 'save-to-clipboard',
		    bindKey: {win: 'Ctrl-S',  mac: 'Command-S'},
		    exec: function(editor) {}
		},
		increaseFontSize: {
			name: 'increase-font',
			bindKey: {mac: 'Command-]'},
			exec: function(editor) {
				var id = editor.container.id;
				var	oldFontSize = AJS.$("#" +id)[0].style.fontSize || "12px",
					newFontSize = oldFontSize.split("px")[0]-0+2;

				AJS.$("#" +id)[0].style.fontSize=newFontSize + "px";
			}
		},
		decreaseFontSize: {
			name: 'decrease-font',
			bindKey: {mac: 'Command-['},
			exec: function(editor) {
				var id = editor.container.id,
					oldFontSize = AJS.$("#" +id)[0].style.fontSize || "12px",
					newFontSize = Math.max(oldFontSize.split("px")[0]-2, 2);
				AJS.$("#" +id)[0].style.fontSize=newFontSize + "px";
			}
		},
		defaultFontSize: {
			name: 'default-font',
			bindKey: {mac: 'Command-0'},
			exec: function(editor) {
				var id = editor.container.id;
				AJS.$("#" +id)[0].style.fontSize= "12px";
			}	
		},
		runWithJs: {
			name: 'Run with JS',
			bindKey: {win: 'Ctrl-J', mac: 'Command-J'},
			exec: function(editor) {
				AJS.$('body').trigger('runJavascript');
			}
		}
	},

    macro: "",
    macroStartCounter: 0,
    macroEndCounter: 0,
    macroStart: false,

	init: function() {
		var editors = {};

		_.each(Editors.config, function(config, key) {
			var editor = ace.edit(config.id);
			editor.setTheme("ace/theme/clouds");
			editor.getSession().setMode(config.mode);
			editor.commands.addCommand(Editors.commands.save);
			editor.commands.addCommand(Editors.commands.increaseFontSize);
			editor.commands.addCommand(Editors.commands.decreaseFontSize);
			editor.commands.addCommand(Editors.commands.defaultFontSize);
			editor.commands.addCommand(Editors.commands.runWithJs);
			editor.setShowPrintMargin(false);
			editor.resize();
			editors[key] = editor;			
		});
		//Set the html theme again as it is the first in the chain and doesn't set propertly in IE
		editors.html.setTheme("ace/theme/clouds");

        this._bindMacroHandler(editors, "html");
//        this._bindMacroHandler(editors.js, "js");

		return editors;
	},

    _repeatString: function(string, number) {
        return new Array( number + 1 ).join( string );
    },

    _bindMacroHandler: function(editors, type) {
        //Handle macro input/paste. Usage {{component}}. Ie {{dialog}}
        var macroKeyStart = "{";
        var macroKeyEnd = "}";
        var beginMacroAtThisCount = 2;
        var macroStart = Editors._repeatString(macroKeyStart, beginMacroAtThisCount);
        var macroEnd = Editors._repeatString(macroKeyEnd, beginMacroAtThisCount);

        var thisEditor = editors[type];

        thisEditor.on("blur", function() {
            Editors._resetMacroData();
        });

        var allowableMacro = [];
        for(i in SANDBOX.Library.components){
        	allowableMacro.push(i);
        }

        thisEditor.on("change", function(e) {
            _.each(allowableMacro, function(macro) {
                var component = SANDBOX.Library.find(macro);
                var toReplace =  macroStart + macro + macroEnd

                var content = thisEditor.getValue();

                if(content.indexOf(toReplace) !== -1) {
                    thisEditor.replaceAll("", {
                        needle: toReplace
                    });
                    thisEditor.insert(component[type]);

                    var otherEditorType = (type == "html") ? "js" : "html";
                    var otherEditor = editors[otherEditorType];
                    var lastRow = otherEditor.getLastVisibleRow() + 1;

                    otherEditor.gotoLine(lastRow, 0, true);
                    otherEditor.insert(component[otherEditorType]);
                }
            });

//            var text = e.data.text;
//
//            if(text === undefined || text === null) {
//                return;
//            }
//
//            //Cue to immediately terminate macro recording
//            if(text.indexOf("\n") !== -1 || text.indexOf(" ") !== -1 || e.data.action === "removeText") {
//                Editors._resetMacroData();
//
//            //Cue to start recording macro input
//            } else if(text.indexOf(macroKeyStart) !== -1 && Editors.macroStartCounter < beginMacroAtThisCount) {
//                Editors.macroStartCounter++;
//                if(Editors.macroStartCounter >= beginMacroAtThisCount) {
//                    Editors.macroStart = true;
//                }
//
//            //Cue to stop recording macro input and insert data
//            } else if(text.indexOf(macroKeyEnd) !== -1) {
//                Editors.macroEndCounter++;
//                if(Editors.macroEndCounter >= beginMacroAtThisCount) {
//                    console.log("Inserting: " + Editors.macro);
//                    var component = Components.get(Editors.macro);
//
//                    //THIS IS ANNOYING. REPLACE AUTOMATICALLY SELECTS THE NEW TEXT
//                    //DAMN YOU ACE EDITOR
//                    thisEditor.replace("", {
//                        needle: macroStart + Editors.macro + macroEnd
//                    });
//                    thisEditor.insert(component[type]);
//                    var otherEditorType = (type == "html") ? "js" : "html";
//                    editors[otherEditorType].insert(component[otherEditorType]);
//
//                    Editors._resetMacroData();
//                }
//
//            //yay, macro already started, start recording MAN
//            } else if(Editors.macroStart) {
//                Editors.macro += text;
//            }
        });
    },

    _resetMacroData: function() {
        Editors.macro = "";
        Editors.macroStartCounter = 0;
        Editors.macroEndCounter = 0;
        Editors.macroStart = false;
    }

};