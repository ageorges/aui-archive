// Stub SaveCtrl for static sandbox
function SaveCtrl($scope, saveService) {
	$scope.canSave = false;


	var componentId = getQuery("component"),
		component = SANDBOX.Library.find(componentId);
		$scope.updateCurrentComponent(componentId);

	if(component){
		setTimeout(function(){
			$scope.setCode(component.html, component.js, component.css);	
		}, 0);	
	}

	function getQuery(queryId){
		return getQueryParams()[queryId]
	}

	function getQueryParams(){
		var rawQuery = window.location.search,
			pathSplit = rawQuery==""?[]:window.location.search.split("?")[1].split("&"),
			object = {};
		for(i in pathSplit){
			var tempSplit = pathSplit[i].split("=");
			object[tempSplit[0]] = tempSplit[1];
		}
		return object;	
	}

}