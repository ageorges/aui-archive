// this is loaded first so we'll set up the global namespace here

if (typeof SANDBOX === "undefined") {
    SANDBOX = {};
}

var sandboxModule = angular.module('sandbox', []);