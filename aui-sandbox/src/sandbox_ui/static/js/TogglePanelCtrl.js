function TogglePanelCtrl($scope) {
    $scope.panels = [
		{
			id: 'library',
			title: 'Library',
			displayed: true,
			left: '0'
			// width: '150px'
		},
		{
			id: 'html',
			title: 'HTML',
			displayed: true,
			left: '20%',
			width: '20%'

		},
		{
			id: 'js',
			title: 'Javascript',
			displayed: true,
			left: '40%',
			width: '20%'
		},
		{
			id: 'css',
			title: 'CSS',
			displayed: false,
			left: '60%',
			width: '20%'
		},
		{
			id: 'output',
			title: 'Preview',
			displayed: true,
			left: '80%',
			width: '20%'
		}
	];

	var setStretchWidth = function() {
        //visible stretch containters

        // Note: left nav width doesn't seem to be working
        var sidebarDefaultWidth = AJS.$("section.left-nav").width(),
        	windowWidth = AJS.$(window).width(),
        	visibleStretchPanels = $scope.getVisiblePanels(),
            numVisibleStretchPanels = visibleStretchPanels.length,
            isLeftNavVisible = $scope.isLibraryVisible();
            
        if (isLeftNavVisible) {        
            windowWidth -= sidebarDefaultWidth;
            numVisibleStretchPanels -= 1;
        }


        // Assuming left nav width is 20% at start..
        var panelWidths = Math.floor(windowWidth / numVisibleStretchPanels),
            left = 0;
        _.each(visibleStretchPanels, function(panel, index){
        	panel.left = left + "px";
        	panel.width = panelWidths + "px";
        	// right = pageWidth - left;
        	if (panel.id === 'library') {
        		left = sidebarDefaultWidth;
        		panel.width = sidebarDefaultWidth + "px";
        	} else {
        		left += panelWidths;
        	}
        });
    }

	$scope.getPanelById = function(panelId) {
		return _.find($scope.panels, function(panel) {
			return panel.id === panelId;
		});
	};

	$scope.togglePanel = function(panelId) {
		var panel = $scope.getPanelById(panelId);
		panel.displayed = !panel.displayed;
		setStretchWidth();
		_.defer(function() {
			_.each($scope.editors, function(editor) {
				editor.resize();
			});
		});
		if(localStorage){
        	localStorage.setItem("toggles", JSON.stringify($scope.panels));
		}
	};

	$scope.isLibraryVisible = function(){
		return $scope.getPanelById('library').displayed;
	};

	$scope.getVisiblePanels = function(){
		return _.filter($scope.panels, function(panel) {
			return panel.displayed;
		});
	};

    setStretchWidth();
    
    if(localStorage){ //feature checking for local storage
	    var lstoggles = localStorage.getItem("toggles");
	    if(lstoggles === undefined || lstoggles === null) {
	        var store = JSON.stringify($scope.panels);
	        localStorage.setItem("toggles", store);
	    } else {
	        $scope.panels = JSON.parse(lstoggles);
	        setStretchWidth();
	    }
	}
}