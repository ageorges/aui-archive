package com.atlassian.aui

import org.apache.maven.wagon.Wagon
import org.apache.maven.wagon.AbstractWagon
import org.apache.maven.wagon.providers.webdav.WebDavWagon
import org.apache.maven.wagon.authentication.AuthenticationInfo
import org.apache.maven.wagon.repository.Repository
import org.apache.maven.settings.Settings
import org.apache.maven.execution.MavenSession

/**
 * Maven2 mojo for generating a new aui-component.xml
 */
class UploadDocs
{

    /**
    * The Maven Session Object
    */
    protected MavenSession session;
    /**
    * The base directory of the project
    */
    private String basedirectory

    /**
    * The current version of the project
    */
    private String currentversion

    /**
    * Whether or not to actually release the docs.
    */
    private boolean releaseLatestDocs

    File templatesFolder
    File project

    org.slf4j.Logger LOG

    void execute()
    {
        //----- UPLOAD DOCUMENTATION ------

        Repository docsAtlassian = new Repository("atlassian-documentation", "https://docs.atlassian.com/aui")
        WebDavWagon wagon = new WebDavWagon();
        AuthenticationInfo auth = new AuthenticationInfo();

        //Authentication
        auth.password = session.settings.getServer("atlassian-documentation").getPassword()
        auth.setUserName session.settings.getServer("atlassian-documentation").getUsername()

        LOG.info("Connecting to https://docs.atlassian.com...")
        //connect to docs.atlassian
        wagon.connect(docsAtlassian, auth);
        wagon.openConnection();
        LOG.info("Connected!")

        //upload AUI Source files
        File auiFlatpackDirectory = new File("${basedirectory}/aui-flat-pack/target/flatpack")

        LOG.info("Uploading AUI Flatpack Files...")
        uploadDirectory auiFlatpackDirectory, "${currentversion}", "", wagon

        LOG.info("AUI Flatpack for v${currentversion} was uploaded successfully!")
        // Release the latest docs
        if (releaseLatestDocs)
        {
          uploadDirectory(auiFlatpackDirectory, "latest", "", wagon);
        }
        //close the wagon connection
        wagon.closeConnection()
    }

    private void uploadDirectory(File directory, String version, String destination, WebDavWagon wagon){
        String uploadDestination = version + "/" +  destination

        directory.eachFile {
            if(it.isDirectory()){
                uploadDirectory(it, version, destination + it.getName() + "/", wagon)
            } else {
                LOG.info("Uploading " + it.getName() +" >>>>>> " + wagon.getRepository().getUrl() +"/" + uploadDestination + it.getName())
                wagon.put(it, uploadDestination + "/" + it.getName())
            }
        }
    }
}

/* Run the mojo directly */
def ud = new UploadDocs()
ud.session = session
ud.LOG = log

ud.basedirectory = project.parent.basedir.path + "/"
ud.currentversion = project.version
ud.releaseLatestDocs = Boolean.valueOf(project.properties['release.latest.docs'])

ud.execute()
